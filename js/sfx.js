(function ($, Drupal, enquire, window, document, undefined) {

  Drupal.behaviors.MainMenuDropDown = {

    attach: function (context) {
      var $main_menu = $('#main-menu .menu-name-main-menu > .menu', context);

      $main_menu.wwuDropdown();

      enquire.register('screen and (min-width: 800px)', {

        match : function () {
          $main_menu.wwuDropdown('enable');
        },

        unmatch : function () {
          $main_menu.wwuDropdown('disable');
        }

      });
    }

  };

}) (jQuery, Drupal, enquire, this, this.document);
